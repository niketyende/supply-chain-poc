/*jslint node: true, nomen: true*/
"use strict";
var path = require("path");
var networkClient = require(path.join(
  "..",
  "lib",
  "invoke_client",
  "invokeNetworkClient.js"
));
var client = require(path.join(
  "..",
  "lib",
  "invoke_client",
  "queryNetworkClient.js"
));

module.exports = function(logger, config) {
  var funcs = {};

  funcs.generateSellerView = function(shipment, cb) {
    var requestParams = {};
    requestParams["funcName"] = "generateSellerView";
    requestParams["args"] = shipment;
    console.log("requestParams : ", requestParams);

    client.queryClient(requestParams, function(err, list) {
      if (err) {
        cb(err, null);
      } else {
        logger.info("Returning shipment list for seller ", list);
        cb(null, list);
      }
    });
  };

  funcs.generateLogisticsView = function(shipment, cb) {
    var requestParams = {};
    requestParams["funcName"] = "generateLogisticsView";
    requestParams["args"] = shipment;
    console.log("requestParams : ", requestParams);

    client.queryClient(requestParams, function(err, list) {
      if (err) {
        cb(err, null);
      } else {
        logger.info("Returning shipment list for logistics provider ", list);
        cb(null, list);
      }
    });
  };

  funcs.generateBuyerView = function(shipment, cb) {
    var requestParams = {};
    requestParams["funcName"] = "generateBuyerView";
    requestParams["args"] = shipment;
    console.log("requestParams : ", requestParams);

    client.queryClient(requestParams, function(err, list) {
      if (err) {
        cb(err, null);
      } else {
        logger.info("Returning shipment list for buyer ", list);
        cb(null, list);
      }
    });
  };

  funcs.getTimeRasterByShipmentID = function(shipmentIDValue, cb) {
    //In this case args passed is a query string
    // var requestParams =
    var selector = {
      selector: { docType: "timeRaster", shipmentID: shipmentIDValue },
      use_index:["_design/indexTimeRasterDoc","indexTimeRaster"]
    };
    var requestParams = {};
    requestParams["funcName"] = "queryState";
    requestParams["args"] = selector;
    console.log("requestParams : ", requestParams);

    client.queryClient(requestParams, function(err, list) {
      if (err) {
        cb(err, null);
      } else {
        logger.info("Returning timeRaster list ", list);
        cb(null, list);
      }
    });
  };

  funcs.getHistoryDetails = function(shipmentID, cb) {
    //In this case args passed is a query string
    // var requestParams =
    
    var requestParams = {};
    requestParams["funcName"] = "getHistoryForShipment";
    requestParams["args"] = shipmentID;
    console.log("requestParams : ", requestParams);

    client.queryClient(requestParams, function(err, historyList) {
      if (err) {
        cb(err, null);
      } else {
        logger.info("Returning historical records for shipment ", historyList);
        cb(null, historyList);
      }
    });
  };

  funcs.createActor = function(actor, cb) {
    //Passing a actor object from UI to Backend
    var requestParams = {};
    console.log("actor ", actor);
    requestParams["funcName"] = "createActor";
    requestParams["args"] = actor;
    console.log("requestParams : ", requestParams);

    networkClient.invokeClient(requestParams, function(err, data) {
      if (err) {
        cb(err, null);
      } else {
        logger.info("New actor added: ", data);
        cb(null, data);
      }
    });
  };

  funcs.createShipment = function(shipment, cb) {
    //Passing a shipment object from UI to Backend
    var requestParams = {};
    console.log("shipment ", shipment);
    requestParams["funcName"] = "createShipment";
    requestParams["args"] = shipment;
    console.log("requestParams : ", requestParams);

    networkClient.invokeClient(requestParams, function(err, data) {
      if (err) {
        cb(err, null);
      } else {
        logger.info("New shipment added: ", data);
        cb(null, data);
      }
    });
  };

  funcs.updateShipmentStatus = function(shipmentStatus, cb) {
    //Passing a shipmentStatus object from UI to Backend
    var requestParams = {};
    console.log("shipmentStatus ", shipmentStatus);
    requestParams["funcName"] = "updateShipmentStatus";
    requestParams["args"] = shipmentStatus;
    console.log("requestParams : ", requestParams);

    // cb(null, requestParams);
    networkClient.invokeClient(requestParams, function(err, data) {
      if (err) {
        cb(err, null);
        console.log("error");
      } else {
        logger.info("registered new contract : ", data);
        cb(null, data);
        console.log("success");
      }
    });
  };

  funcs.addTimeRasterDetail = function(timeRasterObject, cb) {
    //Pushing timeRaster object from external api to Backend
    var requestParams = {};
    console.log("timeRasterObject ", timeRasterObject);
    requestParams["funcName"] = "addTimeRasterDetail";
    requestParams["args"] = timeRasterObject;
    console.log("requestParams : ", requestParams);

    networkClient.invokeClient(requestParams, function(err, data) {
      if (err) {
        cb(err, null);
      } else {
        logger.info("Pushed weather report : ", data);
        cb(null, data);
      }
    });
  };

  return funcs;
};
