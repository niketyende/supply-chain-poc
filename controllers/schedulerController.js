/*jslint node: true, nomen: true*/
"use strict";
var path = require("path");
var networkClient = require(path.join(
  "..",
  "lib",
  "invoke_client",
  "invokeNetworkClient.js"
));

module.exports = function(logger, config) {
  var funcs = {};

  funcs.scheduleJob = function(shipmentID, cb) {
    //In this case args passed is a query string
    // var requestParams =
    var currentTemp = genRand(10, 30, 2);

    var timeRasterObject = {};
    timeRasterObject["shipmentID"] = shipmentID;
    timeRasterObject["temperature"] = currentTemp; //this value will be a random value to denote temperature change
    var requestParams = {};
    requestParams["funcName"] = "addTimeRasterDetail";
    requestParams["args"] = timeRasterObject; 
    console.log("requestParams : ", requestParams);
    
    networkClient.invokeClient(requestParams, function(err, data) {
      if (err) {
        cb(err, null);
      } else {
        logger.info("Schedule job data ", data);
        cb(null, data);
      }
    });
  };

  return funcs;
};

function genRand(min, max, decimalPlaces) {  
  var rand = Math.random()*(max-min) + min;
  var power = Math.pow(10, decimalPlaces);
  return Math.floor(rand*power) / power;
}
