/*jslint node: true, nomen: true*/
"use strict";

module.exports = function(express, logger, config) {
  var path = require("path"),
    router = express.Router(),
    mainController = require(path.join("..", "controllers", "mainController"))(
      logger,
      config
    );

  /* GET all the shipments for a seller */
  router.get("/shipment/seller/:sellerID", function(req, res) {
    // console.log(req);
    var sellerID = req.params.sellerID;
    console.log("sellerID ", sellerID);

    var shipment = {};
    shipment["sellerID"] = sellerID;

    mainController.generateSellerView(shipment, function(err, data) {
      if (err) {
        res.status(err.status).json({
          message: err.message
        });
      } else {
        res.status(200).json(data);
      }
    });
  });

  /* GET all the shipments for a logistics provider in In-Transit or Delivered state */
  router.get("/shipment/logistics/:logisticsID", function(req, res) {
    console.log("logistics "+req);
    var logisticsID = req.params.logisticsID;
    console.log("logisticsID ", logisticsID);

    var shipment = {};
    shipment["lgpID"] = logisticsID;

    mainController.generateLogisticsView(shipment, function(err, data) {
      if (err) {
        res.status(err.status).json({
          message: err.message
        });
      } else {
        res.status(200).json(data);
      }
    });
  });

  /* GET all the shipments for a buyer in Delivered state */
  router.get("/shipment/buyer/:buyerID", function(req, res) {
    // console.log(req);
    var buyerID = req.params.buyerID;
    console.log("buyerID ", buyerID);

    var shipment = {};
    shipment["buyerID"] = buyerID;

    mainController.generateBuyerView(shipment, function(err, data) {
      if (err) {
        res.status(err.status).json({
          message: err.message
        });
      } else {
        res.status(200).json(data);
      }
    });
  });

  /* GET all the timeRaster details for a shipmentID, since the start of shipment  */
  router.get("/timeRaster/:shipmentID", function(req, res) {
    // console.log(req);
    var shipmentID = req.params.shipmentID;
    console.log("shipmentID ", shipmentID);

    mainController.getTimeRasterByShipmentID(shipmentID, function(err, data) {
      if (err) {
        res.status(err.status).json({
          message: err.message
        });
      } else {
        res.status(200).json(data);
      }
    });
  });

  /* GET historical details of a shipment */
  router.get("/shipment/history/:shipmentID", function(req, res) {
    // console.log(req);
    var shipmentID = req.params.shipmentID;
    console.log("shipmentID ", shipmentID);

    mainController.getHistoryDetails(shipmentID, function(err, data) {
      if (err) {
        res.status(err.status).json({
          message: err.message
        });
      } else {
        res.status(200).json(data);
      }
    });
  });

  /* Create a new actor */
  router.post("/actor", function(req, res) {
    var actorID = req.body.id;
    var actorType = req.body.type;
    var location = req.body.location;
    
    var actor = {};
    actor["id"] = actorID;
    actor["type"] = actorType;
    actor["location"] = location;
    console.log("actor : ", actor);

    mainController.createActor(actor, function(err, data) {
      if (err) {
        res.status(err.status).json({
          message: err.message
        });
      } else {
        res.status(200).json(data);
      }
    });
  });

  /* Create a new shipment */
  router.post("/shipment", function(req, res) {
    var shipmentID = req.body.shipmentID;
    var sellerID = req.body.sellerID;
    var buyerID = req.body.buyerID;
    var lgpID = req.body.lgpID;

    var shipment = {};
    shipment["shipmentID"] = shipmentID;
    shipment["sellerID"] = sellerID;
    shipment["buyerID"] = buyerID;
    shipment["lgpID"] = lgpID;

    console.log("shipment object : ", shipment);

    mainController.createShipment(shipment, function(
      err,
      data
    ) {
      if (err) {
        res.status(err.status).json({
          message: err.message
        });
      } else {
        res.status(200).json(data);
      }
    });
  });

  /* Update the shipment status */
  router.post("/shipment/status", function(req, res) {
    var shipmentID = req.body.shipmentID;
    var updatedStatus = req.body.updatedStatus;

    var shipmentStatus = {};
    shipmentStatus["shipmentID"] = shipmentID;
    shipmentStatus["updatedStatus"] = updatedStatus;
    
    console.log("shipmentStatus object : ", shipmentStatus);

    mainController.updateShipmentStatus(shipmentStatus, function(
      err,
      data
    ) {
      if (err) {
        res.status(err.status).json({
          message: err.message
        });
      } else {
        res.status(200).json(data);
      }
    });
  });

  /* Pushes temperature details from IOT device or weather forcast endpoint */
  /**
   * this endpoint updates shipment if there is temp breach 
   */
  router.post("/timeRaster", function(req, res) {
    var shipmentID = req.body.shipmentID;
    var temperature = req.body.temperature;

    var timeRasterObject = {};
    timeRasterObject["shipmentID"] = shipmentID;
    timeRasterObject["temperature"] = temperature;

    //Note: date and time details related to timeRaster are captured in the backend

    console.log("timeRasterObject object : ", timeRasterObject);

    mainController.addTimeRasterDetail(timeRasterObject, function(err, data) {
      if (err) {
        res.status(err.status).json({
          message: err.message
        });
      } else {
        res.status(200).json(data);
      }
    });
  });

  return router;
};
