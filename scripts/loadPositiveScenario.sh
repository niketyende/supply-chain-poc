#!/bin/bash
echo "------------------------------------------------------------"
echo -e "\nThis script is to check the +ve schenario of the usecase\n"
echo "------------------------------------------------------------"
echo -e "\nEnroll the actors/users\n"
# Actor : seller/logistics/buyer
curl -X POST http://52.14.191.130:3000/api/actor -H "Content-Type: application/json" -d '{"id":"seller3","type":"seller","location":"bangalore"}' 
curl -X POST http://52.14.191.130:3000/api/actor -H "Content-Type: application/json" -d '{"id":"logistic3","type":"logistics","location":"mumbai"}'
curl -X POST http://52.14.191.130:3000/api/actor -H "Content-Type: application/json" -d '{"id":"buyer3","type":"buyer","location":"pune"}'  

echo -e "\nRegister the shipment3\n"
# When the shipment is created it is with the seller with state "In-Store"
curl -X POST http://52.14.191.130:3000/api/shipment -H "Content-Type: application/json" -d '{"shipmentID":"shipment3","sellerID":"seller3","buyerID":"buyer3","lgpID":"logistic3"}'  

echo -e "\nTimeRaster details when shipment is in store\n"
# add timeRaster details
curl -X POST http://52.14.191.130:3000/api/timeRaster -H "Content-Type: application/json" -d '{"shipmentID":"shipment3","temperature":"19.5"}'  
curl -X POST http://52.14.191.130:3000/api/timeRaster -H "Content-Type: application/json" -d '{"shipmentID":"shipment3","temperature":"17.5"}'  
curl -X POST http://52.14.191.130:3000/api/timeRaster -H "Content-Type: application/json" -d '{"shipmentID":"shipment3","temperature":"16.5"}'  
curl -X POST http://52.14.191.130:3000/api/timeRaster -H "Content-Type: application/json" -d '{"shipmentID":"shipment3","temperature":"19.0"}' 

echo -e "\nSeller hands over the shipment to logistics provider\n"
# shipment status is changed to In-Transit
curl -X POST http://52.14.191.130:3000/api/shipment/status -H "Content-Type: application/json" -d '{"shipmentID":"shipment3","updatedStatus":"In-Transit"}'

echo -e "\nTimeRaster details when shipment is in transit\n"
# add timeRaster details
temp_array=(19.5 18.5 17 16 21 19 18 19)

for i in "${temp_array[@]}"; 
do 
    echo -e "\n"; 
    curl -X POST http://52.14.191.130:3000/api/timeRaster -H "Content-Type: application/json" -d '{"shipmentID":"shipment3","temperature":"'$i'"}' 
done

echo -e "\nLogistics provider delivers the shipment to buyer\n"
# shipment status is changed to Delivered
curl -X POST http://52.14.191.130:3000/api/shipment/status -H "Content-Type: application/json" -d '{"shipmentID":"shipment3","updatedStatus":"Delivered"}'

# Buyer can Accept or Reject the shipment based on whether there was a temperature breach (temp > 20 for more than 30 mins)
echo -e "\nShipment is Accepted by the buyer since there is no temp breach\n"
curl -X POST http://52.14.191.130:3000/api/shipment/status -H "Content-Type: application/json" -d '{"shipmentID":"shipment3","updatedStatus":"Accepted"}'
echo "------------------------------------------------------------"