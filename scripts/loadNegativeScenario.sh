#!/bin/bash
echo "------------------------------------------------------------"
echo -e "\nThis script is to check the -ve schenario of the usecase\n"
echo "------------------------------------------------------------"
echo -e "\nEnroll the actors/users\n"
# Actor : seller/logistics/buyer
curl -X POST http://52.14.191.130:3000/api/actor -H "Content-Type: application/json" -d '{"id":"seller2","type":"seller","location":"chennai"}' 
curl -X POST http://52.14.191.130:3000/api/actor -H "Content-Type: application/json" -d '{"id":"logistic2","type":"logistics","location":"mumbai"}'
curl -X POST http://52.14.191.130:3000/api/actor -H "Content-Type: application/json" -d '{"id":"buyer2","type":"buyer","location":"delhi"}'  

echo -e "\nRegister the shipment2\n"
# When the shipment is created it is with the seller with state "In-Store"
curl -X POST http://52.14.191.130:3000/api/shipment -H "Content-Type: application/json" -d '{"shipmentID":"shipment2","sellerID":"seller2","buyerID":"buyer2","lgpID":"logistic2"}'  

echo -e "\nTimeRaster details when shipment is in store\n"
# add timeRaster details
curl -X POST http://52.14.191.130:3000/api/timeRaster -H "Content-Type: application/json" -d '{"shipmentID":"shipment2","temperature":"16.5"}'  
curl -X POST http://52.14.191.130:3000/api/timeRaster -H "Content-Type: application/json" -d '{"shipmentID":"shipment2","temperature":"17.5"}'  
curl -X POST http://52.14.191.130:3000/api/timeRaster -H "Content-Type: application/json" -d '{"shipmentID":"shipment2","temperature":"16.5"}'  
curl -X POST http://52.14.191.130:3000/api/timeRaster -H "Content-Type: application/json" -d '{"shipmentID":"shipment2","temperature":"20.0"}' 

echo -e "\nSeller hands over the shipment to logistics provider\n"
# shipment status is changed to In-Transit
curl -X POST http://52.14.191.130:3000/api/shipment/status -H "Content-Type: application/json" -d '{"shipmentID":"shipment2","updatedStatus":"In-Transit"}'

echo -e "\nTimeRaster details when shipment is in transit\n"
echo -e "\nIts been a Hot day resulting in high temperatures\n"
# add timeRaster details
temp_array=(19.5 20.5 22 25 30 26 27 28)

for i in "${temp_array[@]}"; 
do 
    echo -e "\n";  
    curl -X POST http://52.14.191.130:3000/api/timeRaster -H "Content-Type: application/json" -d '{"shipmentID":"shipment2","temperature":"'$i'"}' 
done

echo -e "\nLogistics provider delivers the shipment to buyer\n"
# shipment status is changed to Delivered
curl -X POST http://52.14.191.130:3000/api/shipment/status -H "Content-Type: application/json" -d '{"shipmentID":"shipment2","updatedStatus":"Delivered"}'

# Buyer can Accept or Reject the shipment based on whether there was a temperature breach (temp > 20 for more than 30 mins)
echo -e "\nShipment is Rejected by the buyer since there is a temp breach\n"
curl -X POST http://52.14.191.130:3000/api/shipment/status -H "Content-Type: application/json" -d '{"shipmentID":"shipment2","updatedStatus":"Rejected"}'
echo "------------------------------------------------------------"